﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chat : MonoBehaviour
{
    [SerializeField] Text chatText;
    [SerializeField] InputField chatInput;

    Queue<string> chat = new Queue<string>(5);
    float timer = 10;

    public void SendChatMessage(string val)
    {
        if (chat.Count == 5) chat.Dequeue();
        chat.Enqueue(chatInput.text);
        chatInput.text = "";

        chatText.text = string.Join("\n", chat);
    }
    public void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            if (chat.Count > 0)
            {
                chat.Dequeue();
                chatText.text = string.Join("\n", chat);

            }
            timer = 10;
        }

        if (Input.GetKeyDown(KeyCode.Tilde))
            chatInput.gameObject
               .SetActive(!chatInput.gameObject.activeInHierarchy);
    }
}
